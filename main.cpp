#include <QCoreApplication>
#include "moversservice/CMouseService.h"

int main(int argc, char *argv[]) {
    QCoreApplication a(argc, argv);

    CMouseService *ms = new CMouseService( );
    // cursor will move to the top left and try to close the window
    ms->moveToPositionGradually( 1786, 12 );
    ms->doLeftClick();
    a.exec();
}
