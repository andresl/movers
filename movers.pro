#-------------------------------------------------
#
# Project created by QtCreator 2015-06-02T15:51:58
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = movers
CONFIG   += console
CONFIG   -= app_bundle
LIBS += -lXtst -lX11
TEMPLATE = app


SOURCES += main.cpp \
    movers/MoversFactory.cpp \
    movers/CMouseMovers.cpp \
    movers/LinuxMoversFactory.cpp \
    movers/LinuxCMouseMovers.cpp \
    moversservice/CMouseService.cpp

HEADERS += \
    movers/MoversFactory.h \
    movers/CMouseMovers.h \
    movers/LinuxMoversFactory.h \
    movers/LinuxCMouseMovers.h \
    moversservice/CMouseService.h
# movers/WindowsMoversFactory.h \

