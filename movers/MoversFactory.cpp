#include "MoversFactory.h"
#include "LinuxMoversFactory.h"
//include "WindowsMoversFactory.h"
//include "MacOSXMoversFactory.h"

MoversFactory* MoversFactory::createFactory( int type ) {
    MoversFactory *moversFactory = 0;
    switch ( type ) {
      case LINUX:
        moversFactory = new LinuxMoversFactory( );
        break;
       /* 
      case WINDOWS:
        moversFactory = new WindowsMoversFactory( );
        break;
        
      case MACOSX:
        moversFactory = new MacOSXMoversFactory( );
        break;
        */
    }

    return moversFactory;
}
