#ifndef MOVERSFACTORY_H
#define	MOVERSFACTORY_H
#include "CMouseMovers.h"


class MoversFactory  {
 public:
   static const int WINDOWS = 1;
   static const int LINUX = 2;
   static const int MACOSX = 3;

   static MoversFactory *createFactory( int type );
   virtual CMouseMovers *getMoversCMouse( ) = 0 ;
   // virtual CKeyboardMovers  *getMoversCKeyboard() = 0;
  
};


#endif	/* MOVERSFACTORY_H */
