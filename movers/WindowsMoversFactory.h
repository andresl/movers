#ifndef WINDOWSMOVERSFACTORY_H
#include "WindowsCMouseMovers.h"
#include "MoversFactory.h"

class WindowsMoversFactory: public MoversFactory {
public:

  CMouseMovers *getMoversCMouse();

};

#endif	/* WINDOWSMOVERSFACTORY_H */