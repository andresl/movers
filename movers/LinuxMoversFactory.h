#ifndef LINUXMOVERSFACTORY_H
#include "LinuxCMouseMovers.h"
#include "MoversFactory.h"


class LinuxMoversFactory: public MoversFactory {
public:
  CMouseMovers *getMoversCMouse( );
 // CKeyboardMovers *getMoversCKeyboard( );

};

#endif	/* LINUXMOVERSFACTORY_H */