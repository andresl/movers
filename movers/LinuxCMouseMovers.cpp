#include "LinuxCMouseMovers.h"
#include <X11/Xlib.h>
#include <X11/extensions/XTest.h>


using namespace std;

void LinuxCMouseMovers::setPos( int x, int y ) {
  Display *d = XOpenDisplay( NULL );
  XTestFakeMotionEvent( d, 0, x, y, 0 );
  XFlush( d );
  XCloseDisplay( d );
  // delete( d );
  cout << "New Position: " << x << ", " << y << endl;   
}

void LinuxCMouseMovers::leftClick( ) {
    Display *display = XOpenDisplay(NULL);
    XTestFakeButtonEvent(display, 1, True, 0);
    XFlush(display);

    XTestFakeButtonEvent(display, 1, False, 0);
    XFlush(display);
    cout << "Left Click! has just ocurred." << endl;
}

void LinuxCMouseMovers::rightClick( ) {
  cout << "Right Click! has just ocurred." << endl;
}

int LinuxCMouseMovers::queryX( ) {

  Display *dsp = XOpenDisplay( NULL );
  int screenNumber = DefaultScreen(dsp);

  XEvent event;

  XQueryPointer(dsp, RootWindow(dsp, DefaultScreen(dsp)),
      &event.xbutton.root, &event.xbutton.window,
      &event.xbutton.x_root, &event.xbutton.y_root,
      &event.xbutton.x, &event.xbutton.y,
      &event.xbutton.state);
  XFlush( dsp );

  XCloseDisplay( dsp );
  return event.xbutton.x;
}

int LinuxCMouseMovers::queryY( ) {
  Display *dsp = XOpenDisplay( NULL );
  int screenNumber = DefaultScreen(dsp);

  XEvent event;

  XQueryPointer(dsp, RootWindow(dsp, DefaultScreen(dsp)),
      &event.xbutton.root, &event.xbutton.window,
      &event.xbutton.x_root, &event.xbutton.y_root,
      &event.xbutton.x, &event.xbutton.y,
      &event.xbutton.state);
  XFlush( dsp );

  XCloseDisplay( dsp );
  return event.xbutton.y;
}
